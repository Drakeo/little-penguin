#!/bin/bash

# Send a URL of the form littlepenguin://... to Little Penguin.
#

URL="$1"

if [ -z "$URL" ]; then
    echo Usage: $0 littlepenguin://...
    exit
fi

RUN_PATH=`dirname "$0" || echo .`
cd "${RUN_PATH}/.."

exec ./littlepenguin -url \'"${URL}"\'

